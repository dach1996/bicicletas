var express = require('express');
var router = express.Router();
var bicicletaControllerApi = require('../../controllers/API/BicicletaControllerApi');

router.get('/',bicicletaControllerApi.bicicleta_list);
router.get('/find',bicicletaControllerApi.bicicleta_find_code);
router.post('/create',bicicletaControllerApi.bicicleta_create);
router.put('/update',bicicletaControllerApi.bicicleta_update);
router.delete('/delete',bicicletaControllerApi.bicicleta_delete);


module.exports = router;
  